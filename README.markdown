#I-Excel : School management system

Project I-Excel is the private school management system based on Ruby on Rails. It is developed by a team of developers at Guision Technologies. The project was made by Roshan, and is now maintained by him. I-Excel is the ideal solution for schools and campuses that want an easy means to manage all campus records.

The Project I-Excel website is the home to the developer community behind I-Excel project. There you can find forums and bug tracker for I-Excel.

#Demo
A demo website for I-Excel has been set up at demo.interexcelsystem.com. You can log in with following usernames and passwords:

    * As admin -- username - admin, password - admin123
    * As student -- username - 1, password - 1123
    * As employee -- username - E1, password - E1123

#License

I-Excel is released under the GPL License.

#Installation

Visit https://bitbucket.org/Roshan92/i-excel/downloads/required-step1.sh & https://bitbucket.org/Roshan92/i-excel/downloads/fedena-related-step2.sh for detailed installation instruction.

#Community Support:

Visit bitbucket for community support.